import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AttendenceModel } from '../model/attendenceModel';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-attendence-edit',
  templateUrl: './attendence-edit.component.html',
  styleUrls: ['./attendence-edit.component.css']
})
export class AttendenceEditComponent implements OnInit {
  attendence: AttendenceModel={};
  id:number

  selectDisabled:boolean=true;

  constructor(private serviceService: ServiceService,private router:Router,private activatedRoute:ActivatedRoute) { }
  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.serviceService.find(this.id).subscribe((data:AttendenceModel)=>{
      this.attendence = data;
    });
  
  }
  get f(){
    return this.attendence;
  }

  onSubmit(value){
    debugger
    this.attendence=value;
    this.save();
  }
  save(){
    debugger
    console.log(this.attendence)
    this.serviceService.update(this.id, this.attendence).subscribe(res => {
      console.log('Post updated successfully!');
      this.router.navigateByUrl('attedenceList');
 })
}
  }
  // gotoList(){
  //   debugger
  //   this.router.navigate(['attendenceList']);
  // }


