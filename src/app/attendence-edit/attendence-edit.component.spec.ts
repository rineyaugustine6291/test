import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendenceEditComponent } from './attendence-edit.component';

describe('AttendenceEditComponent', () => {
  let component: AttendenceEditComponent;
  let fixture: ComponentFixture<AttendenceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendenceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendenceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
