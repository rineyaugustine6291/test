import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttandenceListComponent } from './attandence-list/attandence-list.component';
import { AttendenceAddComponent } from './attendence-add/attendence-add.component';
import { AttendenceDetailsComponent } from './attendence-details/attendence-details.component';
import { AttendenceEditComponent } from './attendence-edit/attendence-edit.component';

const routes: Routes = [
{ path: '', component: AttandenceListComponent,},
{ path: 'attendenceList', component: AttandenceListComponent },
{ path: 'attendenceAdd', component: AttendenceAddComponent },
{ path: 'attendenceDetails/:id', component: AttendenceDetailsComponent },
{ path: 'attendenceEdit/:id', component: AttendenceEditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
