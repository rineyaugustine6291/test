import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AttendenceModel } from '../model/attendenceModel';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-attendence-details',
  templateUrl: './attendence-details.component.html',
  styleUrls: ['./attendence-details.component.css']
})
export class AttendenceDetailsComponent implements OnInit {

  id: number;
  attendence: AttendenceModel;

  constructor(private route: ActivatedRoute,private router: Router,
    private employeeService: ServiceService) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    
    this.employeeService.find(this.id).subscribe((data: AttendenceModel)=>{
      this.attendence = data;
    });
  }

  back(){
    this.router.navigate(['attendenceList']);
  }
}
