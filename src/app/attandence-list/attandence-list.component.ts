import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AttendenceModel } from 'src/app/model/attendenceModel';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-attandence-list',
  templateUrl: './attandence-list.component.html',
  styleUrls: ['./attandence-list.component.css']
})
export class AttandenceListComponent implements OnInit {
  id:number;
  attendence:AttendenceModel[];

 constructor(private service: ServiceService,private router:Router) { }
    ngOnInit() {
     this.getAllPosts();
    }
    getAllPosts(){
      this.service.getAll().subscribe(data => {
          debugger
        this.attendence = data; 
        
      });
    }

    attendenceDetails(attendence:AttendenceModel,id:number){
      debugger
      this.router.navigate(['/attendenceEdit',id]);

    }
  

}
