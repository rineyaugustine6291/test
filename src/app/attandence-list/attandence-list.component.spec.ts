import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttandenceListComponent } from './attandence-list.component';

describe('AttandenceListComponent', () => {
  let component: AttandenceListComponent;
  let fixture: ComponentFixture<AttandenceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttandenceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttandenceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
