import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  Observable, throwError } from 'rxjs';
import { AttendenceModel } from '../model/attendenceModel';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private baseUrl='http://localhost:3000/attendenceDetails/'
  AttendenceModel:AttendenceModel;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  
  constructor(private http: HttpClient) { }
  getById(id:number): Observable <any>{
    return this.http.get(`${this.baseUrl}/${id}`)

  }
  create(AttendenceModel:object):Observable<AttendenceModel>{
    debugger
    return this.http.post<AttendenceModel>(this.baseUrl,AttendenceModel)
  }
  update(id, post): Observable<AttendenceModel> {
    return this.http.put<AttendenceModel>(this.baseUrl + '/attendenceEdit/' + id, JSON.stringify(post), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  find(id): Observable<AttendenceModel> {
    return this.http.get<AttendenceModel>(this.baseUrl + + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
  getAll():Observable<AttendenceModel[]>{
    debugger
    return this.http.get<AttendenceModel[]>(this.baseUrl);
  }
  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }

}
