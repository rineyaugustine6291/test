export interface AttendenceModel {
    id?:number;
    employeeCode?:string;
    employeeName?:string;
    inTime?:number;
    outTime?:number;
    hoursWorked?:number;
    overTime?:number;
    

}
