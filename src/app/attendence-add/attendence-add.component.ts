import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AttendenceModel } from '../model/attendenceModel';
import { ServiceService } from '../services/service.service';


@Component({
  selector: 'app-attendence-add',
  templateUrl: './attendence-add.component.html',
  styleUrls: ['./attendence-add.component.css'],
  providers:[ServiceService]
})
export class AttendenceAddComponent implements OnInit {
  attendence: AttendenceModel={};

  selectDisabled:boolean=true;

  constructor(private servicesService: ServiceService,private router:Router) { }

  ngOnInit(): void { }

  onSubmit(value){
    debugger
    this.attendence=value;
    this.save();
  }
  save(){
    debugger
    this.servicesService.create(this.attendence).subscribe(data=>{
      console.log(data)
      this.attendence = data;
      debugger
      this.gotoList();
    })
    debugger
    
  }
  gotoList(){
    debugger
    this.router.navigate(['attendenceList']);
  }
  back(){
    this.router.navigate(['attendenceList']);
  }

}
