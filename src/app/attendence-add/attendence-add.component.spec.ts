import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendenceAddComponent } from './attendence-add.component';

describe('AttendenceAddComponent', () => {
  let component: AttendenceAddComponent;
  let fixture: ComponentFixture<AttendenceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendenceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendenceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
