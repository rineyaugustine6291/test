import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AttandenceListComponent } from './attandence-list/attandence-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AttendenceAddComponent } from './attendence-add/attendence-add.component';
import { AttendenceDetailsComponent } from './attendence-details/attendence-details.component';
import { AttendenceEditComponent } from './attendence-edit/attendence-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AttandenceListComponent,
    AttendenceAddComponent,
    AttendenceDetailsComponent,
    AttendenceEditComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
